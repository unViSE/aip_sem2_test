import matplotlib.pyplot as plt
from src.task1.const_values import start, end, step
from src.utils.bechmarks_as_functions import benchmark_integrate_from_to


def main():
    interval, integrated_values = benchmark_integrate_from_to(start, end, step)

    # difference between the true and calculated value of the integral
    # or
    # absolute accuracy
    for i in range(5):
        for j in range(len(integrated_values[i])):
            integrated_values[i][j] = (-592.45 - integrated_values[i][j])

    fig, ax = plt.subplots()

    ax.plot(interval, integrated_values[0], label="LeftRectanglesRule")
    ax.plot(interval, integrated_values[1], color="#17becf", label="RightRectanglesRule")
    ax.plot(interval, integrated_values[2], color='b', label="MiddleRectanglesRule")
    ax.plot(interval, integrated_values[3], color='y', label="TrapeziumRule")
    ax.plot(interval, integrated_values[4], color='m', label="SimpsonRule")

    ax.set(title="difference between the true and calculated value",
           xlabel="Number of split points", ylabel="Absolute accuracy")
    ax.grid(), ax.legend()
    plt.show()


if __name__ == "__main__":
    main()
