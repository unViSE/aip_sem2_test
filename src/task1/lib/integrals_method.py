class AbstractIntegrate(object):
    def __init__(self, function, x, h_step):
        self.function = function
        self.x = x
        self.h_step = h_step

    def integrate(self):
        raise NotImplementedError


class LeftRectangles(AbstractIntegrate):
    def integrate(self):
        return self.function(self.x)


class RightRectangles(AbstractIntegrate):
    def integrate(self):
        return self.function(self.x + self.h_step)


class MiddleRectangles(AbstractIntegrate):
    def integrate(self):
        return self.function(self.x + self.h_step/2)


class Trapezium(AbstractIntegrate):
    def integrate(self):
        return (self.function(self.x) + self.function(self.x+self.h_step)) * 1/2


class Simpson(AbstractIntegrate):
    def integrate(self):
        return (self.function(self.x) +
                4*self.function(self.x + self.h_step/2) +
                self.function(self.x+self.h_step)) * 1/6
