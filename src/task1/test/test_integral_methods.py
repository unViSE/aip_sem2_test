from math import isclose
from src.task1.lib.callable_Integral import Integral
from src.task1.const_values import function


def test_left_rectangle_rule():
    res = Integral(function).run(10, 20, 10, method_index=0)
    assert isclose(res, -488.16801151036407, abs_tol=1.e-10)


def test_right_rectangle_rule():
    res = Integral(function).run(10, 20, 10, method_index=1)
    assert isclose(res, -717.2013448436974, abs_tol=1.e-10)


def test_middle_rectangle_rule():
    res = Integral(function).run(10, 20, 10, method_index=2)
    assert isclose(res, -587.4005904473988, abs_tol=1.e-10)


def test_trapezium_rule():
    res = Integral(function).run(10, 20, 10, method_index=3)
    assert isclose(res, -602.6846781770307, abs_tol=1.e-10)


def test_simpson_rule():
    res = Integral(function).run(10, 20, 10, method_index=4)
    assert isclose(res, -592.4952863572762, abs_tol=1.e-10)
