import matplotlib.pyplot as plt
from src.task2.const_values import START, END, STEP, PI
from src.utils.bechmarks_as_functions import benchmark_simulation_pi


interval, calculated_pi_values = benchmark_simulation_pi(START, END, STEP)

fig, ax = plt.subplots(figsize=(15, 8))

ax.plot(interval, calculated_pi_values, color="#1f77b4")
ax.axhline(y=PI, linestyle='-', color='b')

ax.set(title="PI value real and calculated",
       xlabel="Number of points", ylabel="PI value")
ax.grid()
plt.show()
