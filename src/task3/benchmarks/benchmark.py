import matplotlib.pyplot as plt
from src.task3.const_values import DATA, WINDOW_SIZE
from src.task3.lib.filters import (SMAFilter, EMAFilter, MedianFilter)


def main():
    x_axis = [i for i in range(len(DATA))]
    sma = SMAFilter(DATA, WINDOW_SIZE).filter()
    ema = EMAFilter(DATA, WINDOW_SIZE).filter()
    median_filter = MedianFilter(DATA, WINDOW_SIZE).filter()

    fig, axes = plt.subplots(figsize=(15, 9), dpi=80)

    axes.plot(x_axis, DATA, label='OriginData', linewidth=2)
    axes.plot(x_axis, sma, linestyle='--', label='SMA')
    axes.plot(x_axis, ema, linestyle='--', label='EMA')
    axes.plot(x_axis, median_filter, linestyle='--', label='MedianFilter')

    axes.set(title=f"Comparing Filters with window size {WINDOW_SIZE}",
             xlabel="Size of array", ylabel="Value")

    axes.legend()
    plt.show()


if __name__ == "__main__":
    main()
