from src.task1.const_values import function, a, b
from src.task1.lib.callable_Integral import Integral
from src.task2.lib.MonteCarloPI import MonteCarloPI


def benchmark_integrate_from_to(start, end, step):
    integrated_values = ([], [], [], [], [])
    interval = []

    for i in range(start, end, step):

        integrated_values[0].append(Integral(function).run(a, b, i))
        integrated_values[1].append(Integral(function).run(a, b, i, method_index=1))
        integrated_values[2].append(Integral(function).run(a, b, i, method_index=2))
        integrated_values[3].append(Integral(function).run(a, b, i, method_index=3))
        integrated_values[4].append(Integral(function).run(a, b, i, method_index=4))

        interval.append(i)
        print(f"Interval for integration is [{i} : {end}] ")
        print(f"Integrated value for LeftRectanglesRule is {integrated_values[0][-1]}\n"
              f"Integrated value for RightRectanglesRule is {integrated_values[1][-1]}\n"
              f"Integrated value for MiddleRectanglesRule is {integrated_values[2][-1]}\n"
              f"Integrated value for TrapeziumRule is {integrated_values[3][-1]}\n"
              f"Integrated value for SimpsonRule is {integrated_values[4][-1]}\n")
    return interval, integrated_values


def benchmark_simulation_pi(start, end, step):
    pi_values, interval = [], []

    for i in range(start, end, step):

        pi_values.append(MonteCarloPI(i).simulate())
        interval.append(i)

        print(f"PI value at {i} points is {pi_values[-1]}")
    return interval, pi_values
